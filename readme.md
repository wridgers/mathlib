# mathlib - a Java framework for pure mathematics

mathlib is an attempt to build a workable pure mathematics framework
in the Java programming language with strong emphasis on minimal
code reuse, collaboration, and simplicity.
