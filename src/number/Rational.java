package core;

/**
 * Rational number type
 * 
 * This is an immutable data type, hence numerator and denominator
 * are final. Changing them will result in a compile-time error.
 * 
 * @author 	Will Ridgers
 */
public class Rational implements MathsObject {
	/**
	 * Rational numbers numerator
	 */
	private final Integer numerator;
	
	/**
	 * Rational numbers denominator
	 */
	private final Integer denominator;
	
	/**
	 * Basic rational number constructor, constructs a rational number with value 0
	 * 
	 */
	public Rational() {
		this.numerator 	 = 0;
		this.denominator = 1;
	}
	
	/**
	 * Rational number constructor, denominator is assumed to be 1
	 * 
	 * @param num 	value of numerator
	 */
	public Rational(Integer num) {
		this.numerator 	 = num;
		this.denominator = 1;
	}
	
	/**
	 * Standard rational number constructor
	 * 
	 * @param num 		value of numerator
	 * @param denom 	value of denominator
	 */
	public Rational(Integer num, Integer denom) {
		// assertions are nicer than exceptions
		assert denom != 0 : "Denominator cannot be zero.";
		
		// fix negatives, so denominator is always positive
		if (denom < 0) {
			num 	*= -1;
			denom 	*= -1;
		}
				
		// done
		if (denom != 1) {
			Integer gcd = gcd(num, denom);
			
			num  	/= gcd;
			denom 	/= gcd;
		}
		
		numerator 	= num;
		denominator = denom;
	}
	
	public Integer getNum() {
		return numerator;
	}
	
	public Integer getDenom() {
		return denominator;
	}

	/**
	 * Indicates whether some other object is "equal to" this one.
	 * 
	 * @param x		Rational to check against
	 * @return		true if both objects are equal, otherwise false
	 */
	public boolean equals(Rational x) {
		if (numerator == x.numerator
				&& denominator == x.denominator)
			return true;
		
		return false;
	}
	
	/**
	 * Indicates whether this object is equal to an integer
	 * 
	 * @param x		Integer to check against
	 * @return		true if both objects are equal, otherwise false
	 */
	public boolean equals(Integer x) {
		return this.equals(new Rational(x));
	}
	
	/**
	 * Add two Rational objects together
	 * 
	 * @param x 	rational to be added
	 * @return		sum of two rationals
	 */
	public Rational add(Rational x) {
		return new Rational(
			numerator * x.denominator + denominator * x.numerator,
			denominator * x.denominator
		);
	}

	/**
	 * Subtract one rational from another
	 * 
	 * @param x 	rational to be subtracted
	 * @return		rational minus rational x
	 */
	public Rational subtract(Rational x) {
		return new Rational(
			numerator * x.denominator - denominator * x.numerator,
			denominator * x.denominator
		);
	}
	
	/**
	 * Get negative of rational
	 * 
	 * @return 	Negative of rational
	 */
	public Rational negative() {
		return new Rational(
				-numerator,
				denominator
		);
	}
	
	/**
	 * Multiple by Rational
	 * 
	 * @param x		Rational to multiply by
	 * @return		result of multiplication
	 */
	public Rational multiply(Rational x) {
		return new Rational(
			numerator * x.numerator,
			denominator * x.denominator
		);
	}
	
	/**
	 * Multiply by Integer
	 * 
	 * @param x		value to multiply by
	 * @return		result of multiplication
	 */
	public Rational multiply(Integer x) {
		return new Rational(
			numerator * x,
			denominator * x
		);
	}
	
	/**
	 * Divide by Rational
	 * 
	 * @param x		Rational to divide by
	 * @return		result of division
	 */
	public Rational divide(Rational x) {
		return new Rational(
			numerator * x.denominator,
			denominator * x.numerator
		);
	}
	
	/**
	 * Divide by Integer
	 * 
	 * @param x		Integer to divide by
	 * @return		result of division
	 */
	public Rational divide(Integer x) {
		return new Rational(
			numerator,
			denominator * x
		);
	}
	
	@Override
	public String getObjectName() {
		return "RationalNumber";
	}
	
	/**
	 * Get value of rational as fraction
	 * 
	 * @return string representation of value
	 */
	public String toString() {		
		if (denominator == 1)
			return numerator.toString();
		else
			return "(" + numerator + "/" + denominator + ")";
	}
	
	/**
	 * Check if the rational is prime
	 * 
	 * @return 	true if the object is prime, otherwise false
	 */
	public boolean isPrime() {
		if (denominator != 1 && denominator != -1)
			return false;
		
		// it remains to show this.numerator is prime
		// first check it's not divisible by 2.
		if (numerator % 2 == 0)
			return false;
		
		// check it's not divisible by 3,5,7,...
		// skip odd numbers because it's faster
		for (Integer x = 3; x < numerator; x += 2)
			if (numerator % x == 0)
				return false;
		
		return true;
	}
	
	/**
	 * Check if the rational is an integer
	 * 
	 * @return 	true if the rational is an integer, otherwise false
	 */
	public boolean isInteger() {
		if (numerator % denominator == 0)
			return true;
		
		return false;
	}

	/**
	 * Recursive implementation of Euclid's Algorithm, for simplifying fractions.
	 * 
	 * @param a		First integer
	 * @param b		Second integer
	 * @return		GCD of a and b
	 */
    private static Integer gcd(Integer a, Integer b) {
        if (b == 0) 
        	return a;
        else 
        	return gcd(b, a % b);
    }

}
