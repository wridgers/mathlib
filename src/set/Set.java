package set;

import java.util.Random;

import core.Complex;
import core.MathsObject;
import core.Rational;

/**
 * The base interface for a set
 * 
 * @author 	Will Ridgers
 */
public interface Set<E> extends MathsObject {

	/**
	 * Set of natural numbers including zero
	 * 
	 * @author 	Will Ridgers
	 */
	class NaturalNumbersWithZero implements Set<Rational> {
		@Override
		public String getObjectName() { return "NaturalsWithZero"; }

		@Override
		public boolean contains(Rational x) {
			if (x.getNum() >= 0 && x.isInteger())
				return true;
			
			return false;
		}

		@Override
		public Integer cardinality() { return null; }

		@Override
		public Rational getRandomElement() {
			// Java's random number generator
			Random randomGenerator = new Random();
			
			return new Rational(randomGenerator.nextInt(Integer.MAX_VALUE));
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}
	
	/**
	 * Set of natural numbers excluding zero
	 * 
	 * @author 	Will Ridgers
	 */
	class NaturalNumbersWithoutZero extends NaturalNumbersWithZero {

		@Override
		public String getObjectName() { return "NaturalsWithoutZero"; }

		@Override
		public boolean contains(Rational x) {
			if (x.getNum() > 0 && x.isInteger())
				return true;
			
			return false;
		}

		@Override
		public Rational getRandomElement() {
			// Java's random number generator
			Random randomGenerator = new Random();
			
			return new Rational(randomGenerator.nextInt(Integer.MAX_VALUE)+1);
		}
	}

	/**
	 * Alias for NaturalNumbersWithZero
	 * 
	 * @author 	Will Ridgers
	 */
	class NaturalNumbers extends NaturalNumbersWithZero {};
	
	/**
	 * Set of rational numbers
	 * 
	 * @author Will Ridgers
	 */
	class RationalNumbers implements Set<Rational> {

		@Override
		public String getObjectName() { return "Rationals"; }

		@Override
		public boolean contains(Rational x) { return true; }
        public boolean contains(Integer x) { return true; }
        public boolean contains(Complex x) {
            if (!x.getImag().equals(0))
                return false;

            return true;
        }

		@Override
		public Integer cardinality() { return null; }

		@Override
		public Rational getRandomElement() {
			// Java's random number generator
			Random randomGenerator = new Random();
						
			return new Rational(
				randomGenerator.nextInt(Integer.MAX_VALUE),
				randomGenerator.nextInt(Integer.MAX_VALUE)
			);
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}
	
	/**
	 * Set of imaginary numbers
	 * 
	 * @author Will Ridgers
	 */
	class ImaginaryNumbers implements Set<Complex> {

		@Override
		public String getObjectName() { return "ImaginaryNumbers"; }

		@Override
		public boolean contains(Complex x) {
			if (x.getReal().equals(0) && !x.getImag().equals(0))
				return true;
			
			return false;
		}
		public boolean contains(Rational x) { return false; }
		public boolean contains(Integer x) { return false; }
		
		@Override
		public Integer cardinality() { return null; }

		@Override
		public Complex getRandomElement() {
            // TODO: not implemented yet
			return null;
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}
	
	/**
	 * Set of complex numbers
	 * 
	 * @author 	Will Ridgers
	 */
	class ComplexNumbers implements Set<Complex> {

		@Override
		public String getObjectName() { return "ComplexNumbers"; }

		@Override
		public boolean contains(Complex x) { return true; }
		public boolean contains(Rational x) { return true; }
		public boolean contains(Integer x) { return true; }

		@Override
		public Integer cardinality() { return null; }

		@Override
		public Complex getRandomElement() {
			return new Complex();
		}

		@Override
		public boolean isEmpty() {
			return false;
		}
	}
	
	/**
	 * Check if an element is in this set
	 * 
	 * @param x 	Element to check is in set
	 * @return 		true if the set contains the element, otherwise false
	 */
	boolean contains(E x);
	
	/**
	 * Get the size of the set
	 * 
	 * @return 		size of the set as Integer
	 */
	Integer cardinality();
	
	/**
	 * Get a random element of the set
	 * 
	 * @return 		a random element of the set
	 */
	E getRandomElement();
	
	/**
	 * Check if the set is empty
	 * 
	 * @return 	true if the set is empty, otherwise false
	 */
	boolean isEmpty();
	
}
