package set;

import core.MathsObject;

/**
 * Base interface for an element of a set
 * 
 * @author Will Ridgers
 */
public interface Element<T> extends MathsObject {
	
	/**
	 * Check if element is in a set
	 * 
	 * @param y 	set to check if element is in
	 * @return		true if element is in set, otherwise false
	 */
	boolean in (Set<T> y);
}
