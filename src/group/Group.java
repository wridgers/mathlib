package group;

import core.MathsObject;

/**
 * The base interface for a group
 * 
 * @author 	Will Ridgers
 */
public interface Group extends MathsObject {

}
