package ring;

import core.MathsObject;

/**
 * The base interface for a ring
 * 
 * @author 	Will Ridgers
 */
public interface Ring extends MathsObject {

}
