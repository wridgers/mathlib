package graph;

import core.MathsObject;

/**
 * The base interface for a graph
 * 
 * @author 	Will Ridgers
 */
public interface Graph extends MathsObject {

}
