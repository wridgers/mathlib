package core;

/**
 * Core MathsObject type, a superclass for all objects
 * 
 * @author 		Will Ridgers
 */
public interface MathsObject {
	
	/**
	 * Get the name of the object
	 * 
	 * @return 	name of the object as String
	 */
	public String getObjectName();
	
}
